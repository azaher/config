# config
A library that provides a custom CLI function to interpret JSON based configuration.

[![License](https://img.shields.io/badge/license-apache%20v2.0-blue.svg?style=flat-square)](https://opensource.org/licenses/Apache-2.0)
[![GoDoc](https://img.shields.io/badge/godoc-reference-blue.svg?style=flat-square)](https://godoc.org/bitbucket.org/azaher/config)
[![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/azaher/config.svg?style=flat-square)](https://bitbucket.org/azaher/config/addon/pipelines/home)
[![Codecov](https://img.shields.io/codecov/c/bitbucket/azaher/config.svg?style=flat-square)](https://codecov.io/bb/azaher/config)
[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/azaher/config?style=flat-square)](https://goreportcard.com/report/bitbucket.org/azaher/config)

## Documentation
Please refer to the [godoc](https://godoc.org/bitbucket.org/azaher/config) pages for documentation.

## Versioning
This project release version format follows [Semantic Versioning](http://semver.org/).

## Contributing
Pull requests and issue reports are welcomed.

## License
This project is licensed under [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
